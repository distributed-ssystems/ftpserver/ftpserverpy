import socket, os, threading, time, sys

IP = ""
PORT = 4444
ADDR = (IP, PORT)

StrgSvr02UdpServerSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


def restart():
    os.execv(sys.executable, ["python3"] + sys.argv)


################################################################################
# ? A FUCNTION THAT SAVES FILES TO STORAGE-SERVER-02 THROUGH A UDP CONNECTION  #                              #
# ? ACCEPTS FILENAME FROM MAIN NODE SERVER                                     #
# ? CREATES A FILE WITH THE FILENAME                                           #
# ? ACCEPTS FILE IN BYTES FORM UNTIL SPECIAL BYTES ARE READ                    #
# ? THEN REMOVES THE ADDITIONAL BYTES WRITES TO THE FILE                       #
################################################################################


def saveFile():
    try:
        fileName, _ = StrgSvr02UdpServerSocket.recvfrom(1024)
        fileName = fileName.decode()
        receivedFile = open(fileName, "wb")
        data, clientAddr = StrgSvr02UdpServerSocket.recvfrom(2048)
        while data:
            receivedFile.write(data)
            if data[-8:] == b"[TFROVR]":
                break
            data, clientAddr = StrgSvr02UdpServerSocket.recvfrom(2048)

        receivedFile.close()
        print(f"[[[\t {fileName} SAVED TO DISK \t ]]]")
        restart()

    except Exception as e:
        print(e)


####################################################
# ? FUCNTION TO DOWNLOAD FILES TO MAIN NODE SERVER #
# ? USES UDP PROTOCOL                              #
# ? ACCEPTS FILENAME FROM MAIN NODE SERVER         #
# ? READS THE FILE AND SENDS  2048 BYTES A TIM     #
####################################################
def getFileForDownload(clientAddr):
    try:
        fileName, _ = StrgSvr02UdpServerSocket.recvfrom(1024)
        fileName = fileName.decode()

        file = open(fileName, "rb")
        fileContents = file.read(2048)

        while fileContents:
            if StrgSvr02UdpServerSocket.sendto(fileContents, clientAddr):
                fileContents = file.read(2048)
                time.sleep(0.01)

        StrgSvr02UdpServerSocket.sendto(b"[TFROVR]", clientAddr)
        file.close()

        print(f"[[[ \t {fileName} SENT TO MNSVR \t]]]")
        restart()

    except Exception as e:
        print(e)


########################################################################
# ? ENTRY POIN FOR STORAGE-SERVER-02                                  #
# ? LISTENS FOR INCCOMING REQUESTS FROM MAIN NODE SERVER               #
# ? ACCEPTS ACTION TYPE FROM MAIN NODE SERVER                          #
# ? IF ACTION IS UPLOAD CALLS SAVE FUCNTION WITH ITS OWN THREAD        #
# ? IF ACTION IS DOWNLOAD, CALLS DOWNLOAD FUCNTION WITH ITS OWN THREAD #
########################################################################

StrgSvr02UdpServerSocket.bind(ADDR)
print("[[[ \t STRGSVR02 UDP SERVER UP \t]]] \n")


action, clientAddr = StrgSvr02UdpServerSocket.recvfrom(1024)

if action == b"upload":
    saveFileThread = threading.Thread(target=saveFile)
    saveFileThread.start()
elif action == b"download":
    getFileForDownloadThread = threading.Thread(
        target=getFileForDownload, args=(clientAddr,)
    )
    getFileForDownloadThread.start()
