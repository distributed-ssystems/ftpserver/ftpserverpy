############# module imports ##################
import socket, os, datetime, time, sys, threading, random, sqlite3
from multiprocessing import Process

###############################################
##############  HELPER FUCNTIONS ##############
###############################################
loadBalanceThreasholdSize = 5243000

####  A FUCNTION THAT RESTARTS THE CALLING SCRIPT  ####
def restart():
    os.execv(sys.executable, ["python3"] + sys.argv)
    print("restarted")


try:
    dbCon = sqlite3.connect("fileLookup.db", check_same_thread=False)
except Exception as e:
    print(e)


######################################################
# ? RETRIEVES LOCATION OF A FILE FROM LOOKUP TABLE   #
# ? TAKES FILENAME AS AN ARGUMENT                    #
# ? RETURNS EITHER LOCATION OF A FILE AS INT OR NONE #
# ? CALLED WHEN A FILE DOWNLOAD REQUEST IS SENT      #
######################################################


def getFileLocation(fileName):
    try:
        results = dbCon.execute(
            "SELECT fileName,storedIn from lookupTable WHERE fileName = ?", (fileName,)
        )

        for row in results:
            if row[0] == fileName:
                return row[1]

    except Exception as e:
        print(e)


####################################################################
# ? ADDS A NEW ENTRY TO THE LOOKUP TABLE                           #
# ? TAKES FILENAME & STORAGE LOCATION AS ARGUMENTS                 #
# ? CALLED WHEN A NEW FILE IS STORED IN ONE OF THE STORAGE SERVERS #
####################################################################


def addNewLookupRecord(name, storage):
    try:
        dbCon.execute(
            "INSERT INTO lookupTable (fileName,storedIn) VALUES (?,?)", (name, storage)
        )
        dbCon.commit()
    except Exception as e:
        print(e)






############### END OF HELPER FUNCTIONS #########################################################

###############################################
#########  MAIN NODE AS A TCP CLIENT ##########
###############################################

mainNodeTcpClientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mainNodeTcpClientSocket.connect(("localhost", 2222))


##############################################################################
# ? A HELPER FUNCTION THAT GETS CALLED TO SAVE A FILE TO STORAGE-SERVER-01   #
# ? IT CONNECTS TO STORAGE-SERVER-01 THROUGH TCP PROTOCOL                    #
# ? ACCEPTS FILENAME AND SENDS THE FILE TO STORAGE-SERVER-01                 #
##############################################################################

def uploadFileToStrgSvr01(fileName):
    try:
        mainNodeTcpClientSocket.send("upload".encode())
        file = open(fileName, "rb")

        mainNodeTcpClientSocket.send(fileName.encode())
        time.sleep(0.01)
        fileData = file.read()

        print("[[[ \t SENDING FILE TO STRGSVR01 \t ]]]")
        mainNodeTcpClientSocket.sendall(fileData)
        mainNodeTcpClientSocket.send(b"[TFROVR]")
        file.close()
        print("[[[ \t FILE SENT \t ]]]")

    except Exception as e:
        print(e)

###################################################################################
# ? A HELPER FUNCTION THAT GETS CALLED TO DOWNLOAD A FILE FROM STORAGE-SERVER-01  #
# ? IT CONNECTS TO STORAGE-SERVER-01 THROUGH TCP PROTOCOL                         #
# ? ACCEPTS FILENAME AS AN ARGUMENT                                                  #
###################################################################################
def downloadFileFromStrg01(fileName):
    try:
        mainNodeTcpClientSocket.send("download".encode())
        time.sleep(.1)
        mainNodeTcpClientSocket.send(fileName.encode())

        receievedFile = open(fileName, "wb")
        receivedBytes = b""

        while True:
            chunk = mainNodeTcpClientSocket.recv(2048)
            if chunk[-8:] == b"[TFROVR]":
                receivedBytes += chunk
                break

            receivedBytes += chunk

        receivedBytes = receivedBytes[:-8]
        receievedFile.write(receivedBytes)
        receievedFile.close()
        print("[[[ \t FILE RECEIVED FROM STRGSVR01 \t ]]]")

    except Exception as e:
        print(e)

###################### END OF MAIN NODE AS A TCP CLIET LOGIC ############################################################################################################


###############################################
#########  MAIN NODE AS A UDP CLIENT ##########
###############################################
ser_IP = "localhost"
ser_PORT = 4444
ser_ADDR = (ser_IP, ser_PORT)
mainNodeUdpClientSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
mainNodeUdpClientSocket.bind(("0.0.0.0", random.randint(7000, 8000)))


##############################################################################
# ? A HELPER FUNCTION THAT GETS CALLED TO SAVE A FILE TO STORAGE-SERVER-02   #
# ? IT CONNECTS TO STORAGE-SERVER-02 THROUGH UDP PROTOCOL                    #
# ? ACCEPTS FILENAME AND SENDS THE FILE TO STORAGE-SERVER-02                 #
##############################################################################
def uploadFileToStrgSvr02(fileName):
    try:
        mainNodeUdpClientSocket.sendto(b"upload", ser_ADDR)
        file = open(fileName, "rb")
        mainNodeUdpClientSocket.sendto(fileName.encode(), ser_ADDR)
        fileContents = file.read(2048)

        while fileContents:
            if mainNodeUdpClientSocket.sendto(fileContents, ser_ADDR):
                fileContents = file.read(2048)

        mainNodeUdpClientSocket.sendto(b"[TFROVR]", ser_ADDR)
        file.close()
        print("[[[ \t SENT TO STRGSVR02 \t ]]]")

    except Exception as e:
        print(e)


###################################################################################
# ? A HELPER FUNCTION THAT GETS CALLED TO DOWNLOAD A FILE FROM STORAGE-SERVER-02  #
# ? IT CONNECTS TO STORAGE-SERVER-01 THROUGH UDP PROTOCOL                         #
# ? ATKES FILENAME AS AN ARGUMENT                                                 #
###################################################################################
def downloadFileFromStrg02(fileName):
    try:
        mainNodeUdpClientSocket.sendto(b"download", ser_ADDR)
        mainNodeUdpClientSocket.sendto(fileName.encode(), ser_ADDR)

        receivedFile = open(fileName, "wb")
        while True:
            data, srvradr = mainNodeUdpClientSocket.recvfrom(2048)
            receivedFile.write(data)
            if data[-8:] == b"[TFROVR]":
                break

        receivedFile.close()
        print("[[[ \t FILE DOWNLOADED FROM STRGSVR02 \t]]]")

    except Exception as e:
        print(e)


#################### END OF MAIN NODE AS A UDP CLIENT LOGIC #########################################################################################################################################3


###############################################
#########  MAIN NODE AS A TCP SERVER ##########
###############################################
mainNodeTcpServerSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mainNodeTcpServerSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
mainNodeTcpServerSocket.bind(("", 1111))

# loadBalanceThreasholdSize = 5243000


###########################################################################
# ? A FUCNTION THAT SAVES FILES TO SERVERS                                #
# ? ACCEPTS FILENAME FROM CLIENT VIA UDP                                  #
# ? CREATES A FILE WITH THE FILENAME                                      #
# ? ACCEPTS FILE IN BYTES FORM UNTIL SPECIAL BYTES ARE READ               #
# ? THEN REMOVES THE ADDITIONAL BYTES WRITES TO THE FILE                  #
# ? CALCULATES THE FILESIZE OF THE FILE                                   #
# ? BASED ON THE SIZE SENDS THE FILE TO EITHER ONE OF THE STORAGE SERVERS #
# ? UPDATES LOOKUPTABLE ACCORDINGLY                                       #
# ? SLEEPS TO MAKE SURE ALL FILE OPERATIONS ARE COMPLETE                  #
# ? FINALLY DELETES THE FILE                                              #
###########################################################################
def saveFileTcp(endUserCon):
    fileName = endUserCon.recv(1024).decode()

    if fileName == "noFile":
        restart()

    receivedFile = open(fileName, "wb")
    receivedBytes = b""
    while True:
        try:
            chunk = endUserCon.recv(2048)
            if chunk[-8:] == b"[TFROVR]":
                receivedBytes += chunk
                break

            receivedBytes += chunk

        except Exception as e:
            print(e)

    receivedBytes = receivedBytes[:-8]
    receivedFile.write(receivedBytes)
    receivedFile.close()

    fileSize = os.path.getsize(fileName)
    if fileSize >= loadBalanceThreasholdSize:
        uploadFileToStrgSvr01(fileName)
        addNewLookupRecord(fileName, 1)
    else:
        uploadFileToStrgSvr02(fileName)
        addNewLookupRecord(fileName, 2)

    time.sleep(1)
    os.remove(fileName)
    restart()


#####################################################################
# ? FUCNTION TO DOWNLOAD FILES TO CLIENTS                           #
# ? ACCEPTS FILENAME FROM CLIENT                                    #
# ? CHECKS IF FILE EXISTS IN LOOKUPTABLE                            #
# ? IF FILE ISNT FOUND SENDS NO                                     #
# ? IF FILE IS FOUND DOWNLOADS TEH FILES FROM THE STORAGE SERVERS   #
# ? THEN SENDS THE FILE TO THE REQUESTING CLIENT                    #
#####################################################################


def getFileForDownloadTcp(endUserCon):
    try:
        fileName = endUserCon.recv(1024).decode()

        fileLocation = getFileLocation(fileName)

        if fileLocation == 1:
            downloadFileFromStrg01(fileName)

            endUserCon.send("yes".encode())
            file = open(fileName, "rb")
            fileData = file.read()

            print("[[[ \t SENDING FILE \t ]]]")
            endUserCon.sendall(fileData)
            endUserCon.send(b"[TFROVR]")

            print(f"[[[ \t FILE SENT TO CLIENT \t ]]]")
            file.close()

            time.sleep(1)
            os.remove(fileName)
            restart()

        elif fileLocation == 2:
            downloadFileFromStrg02(fileName)

            endUserCon.send("yes".encode())
            file = open(fileName, "rb")
            fileData = file.read()

            print("[[[ \t SENDING FILE \t ]]]")
            endUserCon.sendall(fileData)
            endUserCon.send(b"[TFROVR]")

            print(f"[[[ \t FILE SENT TO CLIENT \t ]]]")
            file.close()

            time.sleep(1)
            os.remove(fileName)
            restart()
        else:
            endUserCon.send("no".encode())
            restart()
    except Exception as e:
        print(e)


########################################################################
# ? ENTRY POIN FOR TCPSERVER                                           #
# ? LISTENS FOR INCCOMING REQUESTS FROM CLIENTS                        #
# ? ACCEPTS ACTION TYPE FROM CLIENTS                                   #
# ? IF ACTION IS UPLOAD CALLS SAVE FUCNTION WITH ITS OWN THREAD        #
# ? IF ACTION IS DOWNLOAD, CALLS DOWNLOAD FUCNTION WITH ITS OWN THREAD #
########################################################################

def launchMainNodetcpServer():
    mainNodeTcpServerSocket.listen()
    print("[[[ \t MAIN NODE TCP LISTENING ...  \t]]]")

    while True:
        endUserCon, endUserAddr = mainNodeTcpServerSocket.accept()
        action = endUserCon.recv(1024).decode()

        if action == "upload":
            saveFilesTcpThread = threading.Thread(target=saveFileTcp, args=(endUserCon,)).start()
        
        elif action == "download":
            getFileForDownloadTcpThread = threading.Thread(
                target=getFileForDownloadTcp, args=(endUserCon,)
            ).start()
 
#################### END OF MAIN NODE AS A TCP SERVER LOGIC #########################################################################################################################################


###############################################
#########  MAIN NODE AS A UDP SERVER ##########
###############################################

IP = ""
PORT = 3333
ADDR = (IP, PORT)
# loadBalanceThreasholdSize = 5243000

mainNodeUdpServerSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


###########################################################################
# ? A FUCNTION THAT SAVES FILES TO SERVERS                                #
# ? ACCEPTS FILENAME FROM CLIENT                                          #
# ? CREATES A FILE WITH THE FILENAME                                      #
# ? ACCEPTS FILE IN BYTES FORM UNTIL SPECIAL BYTES ARE READ               #
# ? THEN REMOVES THE ADDITIONAL BYTES WRITES TO THE FILE                  #
# ? CALCULATES THE FILESIZE OF THE FILE                                   #
# ? BASED ON THE SIZE SENDS THE FILE TO EITHER ONE OF THE STORAGE SERVERS #
# ? UPDATES LOOKUPTABLE ACCORDINGLY                                       #
# ? SLEEPS TO MAKE SURE ALL FILE OPERATIONS ARE COMPLETE                  #
# ? FINALLY DELETES THE FILE                                              #
###########################################################################
def saveFileUdp():
    try:
        fileName, _ = mainNodeUdpServerSocket.recvfrom(1024)
        fileName = fileName.decode()

        if fileName == "noFile":
            restart()

        receivedFile = open(fileName, "wb")
        data, clientAddr = mainNodeUdpServerSocket.recvfrom(2048)
        while data:
            receivedFile.write(data)
            if data[-8:] == b"[TFROVR]":
                break
            data, clientAddr = mainNodeUdpServerSocket.recvfrom(2048)

        receivedFile.close()
        print("[[[ \t FILE SAVED \t ]]]]")

        fileSize = os.path.getsize(fileName)

        if fileSize >= loadBalanceThreasholdSize:
            uploadFileToStrgSvr01(fileName)
            addNewLookupRecord(fileName, 1)
        else:
            uploadFileToStrgSvr02(fileName)
            addNewLookupRecord(fileName, 2)

        time.sleep(1)
        os.remove(fileName)
        restart()

    except Exception as e:
        print(e)


#####################################################################
# ? FUCNTION TO DOWNLOAD FILES TO CLIENTS                           #
# ? ACCEPTS FILENAME FROM CLIENT                                    #
# ? CHECKS IF FILE EXISTS IN LOOKUPTABLE                            #
# ? IF FILE ISNT FOUND SENDS NO                                     #
# ? IF FILE IS FOUND DOWNLOADS TEH FILES FROM THE STORAGE SERVERS   #
# ? THEN SENDS THE FILE TO THE REQUESTING CLIENT                    #
#####################################################################


def getFileForDownloadUdp(clientAddr):
    try:
        fileName, _ = mainNodeUdpServerSocket.recvfrom(1024)
        fileName = fileName.decode()

        fileLocation = getFileLocation(fileName)

        if fileLocation == 1:
            downloadFileFromStrg01(fileName)

            mainNodeUdpServerSocket.sendto("yes".encode(), clientAddr)
            file = open(fileName, "rb")
            fileContents = file.read(2048)

            while fileContents:
                if mainNodeUdpServerSocket.sendto(fileContents, clientAddr):
                    fileContents = file.read(2048)
                    time.sleep(0.005)
            
            mainNodeUdpServerSocket.sendto(b"[TFROVR]", clientAddr)
            file.close()

            print("[[[ \t FILE SENT TO USER \t]]]")
            time.sleep(1)
            os.remove(fileName)
            restart()

        if fileLocation == 2:
            downloadFileFromStrg02(fileName)

            mainNodeUdpServerSocket.sendto("yes".encode(), clientAddr)
            file = open(fileName, "rb")
            fileContents = file.read(2048)

            while fileContents:
                if mainNodeUdpServerSocket.sendto(fileContents, clientAddr):
                    fileContents = file.read(2048)
                    time.sleep(1)

            
            mainNodeUdpServerSocket.sendto(b"[TFROVR]", clientAddr)
            file.close()

            print("[[[ \t FILE SENT TO CLIENT \t]]]")
            time.sleep(1)
            os.remove(fileName)
            restart()

        else:
            mainNodeUdpServerSocket.sendto("no".encode(), clientAddr)
            restart()

    except Exception as e:
        print(e)


########################################################################
# ? ENTRY POIN FOR THIS SCRIPT                                         #
# ? LISTENS FOR INCCOMING REQUESTS FROM CLIENTS                        #
# ? ACCEPTS ACTION TYPE FROM CLIENTS                                   #
# ? IF ACTION IS UPLOAD CALLS SAVE FUCNTION WITH ITS OWN THREAD        #
# ? IF ACTION IS DOWNLOAD, CALLS DOWNLOAD FUCNTION WITH ITS OWN THREAD #
########################################################################
def launchMainNodeUdpServer():

    mainNodeUdpServerSocket.bind(ADDR)
    print("[[[ \t MAIN NODE UDP LISTENING ... \t]]] \n")

    action, clientAddr = mainNodeUdpServerSocket.recvfrom(1024)
    if action == b"upload":
        saveFileUdpThread = threading.Thread(target=saveFileUdp).start()
        
    elif action == b"download":
        getFileForDownloadUdpThread = threading.Thread(
            target=getFileForDownloadUdp, args=(clientAddr,)
        ).start()


################## END OF MAIN NODE AS A UDP SERVER LOGIC #################################################################################################



##############################################################################################################################################################################
############################################### ENTRY POINT FOR THE SCRIPT #################################################################################################

# tcpServerProcess = Process(target=launchMainNodetcpServer).start()
# udpServerProcess = Process(target=launchMainNodeUdpServer).start()

tcpServerThread = threading.Thread(target=launchMainNodetcpServer).start()
udpServerThread = threading.Thread(target=launchMainNodeUdpServer).start()

###############################################################################################################################################################################