import socket, os, random, time, datetime

ser_IP = "192.168.53.218"
ser_PORT = 3333
ser_ADDR = (ser_IP, ser_PORT)

endUserUdpClient = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
endUserUdpClient.bind(("0.0.0.0", random.randint(7000, 8000)))

##########################################################################
# ? A FUCNTION THAT UPLOADS FILES VIA UDP CONNECTION TO THE MNSVR       #
# ? ACCEPTS FILE NAME FROM USER                                         #
# ? SENDS THE FILENAME TO SERVER FIRST                                  #
# ? READS THE FILE CONTENTS AND 2048 BYTES AT A TIME                    #                                  #
# ? ATTACHES ADDITIONAL BYTE TO INDICATE COMPLETION OF FILE TRANSFER    #
# ? CALCULATES AND SHOWS ELAPSED  TIME & FILESIZE                       #
##########################################################################


def uploadFileViaUdp():
    fileName = input("FILE NAME:: \n")
    try:
        if os.path.exists(fileName):
            file = open(fileName, "rb")
            fileSize = os.path.getsize(file.name)

            endUserUdpClient.sendto(fileName.encode(), ser_ADDR)
            fileContents = file.read(2048)
            print("[[[ \t SENDING FILE \t]]]")
            startTime = datetime.datetime.now()
            while fileContents:
                if endUserUdpClient.sendto(fileContents, ser_ADDR):
                    print("*", end=" ")
                    fileContents = file.read(2048)

            endUserUdpClient.sendto(b"[TFROVR]", ser_ADDR)
            endTime = datetime.datetime.now()
            file.close()
            print(f"[[[ \t FILE SENT {fileSize} bytes \t]]]")
            print(f"[[ \t ELAPSED TIME {(endTime-startTime)} \t]]]")
            os._exit(0)
        else:
            print("[[[ \t FILE NOT FOUND \t]]]")
            endUserUdpClient.sendto("noFile".encode(), ser_ADDR)
            os._exit(1)

    except Exception as e:
        print(e)


##########################################################################
# ? A FUCNTION THAT DOWNLOADS FILES VIA UDP CONNECTION TO THE MNSVR      #
# ? ACCEPTS FILE NAME FROM USER                                          #
# ? SENDS THE FILENAME TO SERVER FIRST  FILENAME                         #
# ? RECIEVES FILES STATUS FROM MNSVR                                     #
# ? IF FILES ISN'T FOUND, IT EXIST                                       #
# ? IF FILE IS FOUND, RECEIVES BYTES UNTIL THE BYTES INDICATING          #
# ? TRANSFER COMPLETION ARE RECEIVED                                     #
##########################################################################


def downloadFileViaUdp():
    try:
        fileName = input("FILE NAME:: \n")
        endUserUdpClient.sendto(fileName.encode(), ser_ADDR)
        fileStatus, _ = endUserUdpClient.recvfrom(1024)
        fileStatus = fileStatus.decode()
        if fileStatus == "yes":
            receivedFile = open(fileName, "wb")
            print("[[[\t DOWNLOADING FILE ... \t]]]")
            while True:
                data, srvradr = endUserUdpClient.recvfrom(2048)
                receivedFile.write(data)
                print(".", end=" ")
                if data[-8:] == b"[TFROVR]":
                    break

            receivedFile.close()
            print("\n[[[ \t FILE DOWNLOADED \t]]]")
            os._exit(0)
        elif fileStatus == "no":
            print("[[[ \t FILE NOT FOUND \t]]]")

    except Exception as e:
        print(e)


#########################################################################
# ? MAIN MENU THAT LETS USERS CHOOSE AN ACTION                          #
# ? ACCEPTS INPUT AND SENDS AN ACTION MESSAGE TO MNSVR THAT             #
# ? TRIGGERS THE CORREPSONDOING FUNCTION                                #
#########################################################################

exitMenu = False
while not exitMenu:
    print("Menu")
    print("1 - UPLOAD")
    print("2 - DOWNLOAD")
    print("3 - EXIT")
    choice = input(":: ")
    if choice == "1":
        endUserUdpClient.sendto(b"upload", ser_ADDR)
        uploadFileViaUdp()
    elif choice == "2":
        endUserUdpClient.sendto(b"download", ser_ADDR)
        downloadFileViaUdp()
    elif choice == "3":
        print("Exit")
        exitMenu = True
    else:
        print("Invalid Choice")
