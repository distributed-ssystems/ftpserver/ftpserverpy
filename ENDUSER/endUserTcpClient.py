import socket, os, datetime, sys, time

endUserTcpSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
endUserTcpSocket.connect(("192.168.53.218", 1111))


##########################################################################
# ? A FUCNTION THAT UPLOADS FILES VIA TCP CONNECTION TO THE MNSVR       #
# ? ACCEPTS FILE NAME FROM USER                                         #
# ? SENDS THE FILENAME TO SERVER FIRST                                  #
# ? READS THE FILE CONTENTS                                             #
# ? SENDS FILE CONTENT AT ONCE                                          #
# ? ATTACHES ADDITIONAL BYTE TO INDICATE COMPLETION OF FILE TRANSFER    #
# ? CALCULATES AND SHOWS ELAPSED  TIME & FILESIZE                       #
##########################################################################


def uploadFile():
    fileName = input("\n ENTER FILE NAME:: ")

    try:
        if os.path.exists(fileName):
            file = open(fileName, "rb")
            fileSize = os.path.getsize(file.name)

            endUserTcpSocket.send(fileName.encode())
            time.sleep(0.5)
            fileData = file.read()
            print("[[[ \t SENDING FILE \t ]]]")
            startTime = datetime.datetime.now()
            endUserTcpSocket.sendall(fileData)
            endUserTcpSocket.send(b"[TFROVR]")
            endTime = datetime.datetime.now()
            print(f"[[[ \t FILE SENT ... {fileSize} bytes \t ]]]")
            print(f"[[[ \t ELAPSED_TIME... {(endTime-startTime)} \t ]]] ")
            file.close()
            os._exit(0)
        else:
            print("[[[ \t FILE NOT FOUND \t ]]]")
            endUserTcpSocket.send("noFile".encode())
            os._exit(1)

    except Exception as e:
        print(e)


##########################################################################
# ? A FUCNTION THAT DOWNLOADS FILES VIA TCP CONNECTION TO THE MNSVR      #
# ? ACCEPTS FILE NAME FROM USER                                          #
# ? SENDS THE FILENAME TO SERVER FIRST  FILENAME                         #
# ? RECIEVES FILES STATUS FROM MNSVR                                     #
# ? IF FILES ISN'T FOUND, IT EXIST                                       #
# ? IF FILE IS FOUND, RECEIVES BYTES UNTIL THE BYTES INDICATING          #
# ? TRANSFER COMPLETION ARE RECEIVED                                     #
##########################################################################


def downloadFile():
    fileName = input("\n ENTER FILE NAME:: ")
    try:
        endUserTcpSocket.send(fileName.encode())

        fileStatus = endUserTcpSocket.recv(1024).decode()

        if fileStatus == "yes":
            receievedFile = open(fileName, "wb")
            receivedBytes = b""

            while True:
                chunk = endUserTcpSocket.recv(2048)
                if chunk[-8:] == b"[TFROVR]":
                    receivedBytes += chunk
                    break

                receivedBytes += chunk

            receivedBytes = receivedBytes[:-8]
            receievedFile.write(receivedBytes)
            receievedFile.close()
            print("[[[ \t FILE DOWNLOADED \t ]]]")
            os._exit(0)

        else:
            print("FILE NOT FOUND")
            os._exit(0)

    except Exception as e:
        print(e)


#########################################################################
# ? MAIN MENU THAT LETS USERS CHOOSE AN ACTION                          #
# ? ACCEPTS INPUT AND SENDS AN ACTION MESSAGE TO MNSVR THAT             #
# ? TRIGGERS THE CORREPSONDOING FUNCTION                                #
#########################################################################


exitMenu = False
while not exitMenu:
    print("Menu")
    print("1 - UPLOAD")
    print("2 - DOWNLOAD")
    print("3 - EXIT")
    choice = input(":: ")
    if choice == "1":
        endUserTcpSocket.send("upload".encode())
        uploadFile()
    elif choice == "2":
        endUserTcpSocket.send("download".encode())
        downloadFile()
    elif choice == "3":
        print("Exit")
        exitMenu = True
    else:
        print("Invalid Choice")
