import socket, os, threading, sys, time

STRSVR01Socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
STRSVR01Socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
STRSVR01Socket.bind(("", 2222))


def restart():
    os.execv(sys.executable, ["python3"] + sys.argv)


################################################################################
# ? A FUCNTION THAT SAVES FILES TO STORAGE-SERVER-01 THROUGH A TCP CONNECTION  #                              #
# ? ACCEPTS FILENAME FROM MAIN NODE SERVER                                     #
# ? CREATES A FILE WITH THE FILENAME                                           #
# ? ACCEPTS FILE IN BYTES FORM UNTIL SPECIAL BYTES ARE READ                    #
# ? THEN REMOVES THE ADDITIONAL BYTES WRITES TO THE FILE                       #
################################################################################


def saveFile(endUserCon):
    fileName = endUserCon.recv(1024).decode()

    receivedFile = open(fileName, "wb")
    receivedBytes = b""
    while True:
        try:
            chunk = endUserCon.recv(2048)
            if chunk[-8:] == b"[TFROVR]":
                receivedBytes += chunk
                break

            receivedBytes += chunk

        except Exception as e:
            print(e)

    receivedBytes = receivedBytes[:-8]
    receivedFile.write(receivedBytes)
    receivedFile.close()

    print(f"[[[ \t {fileName} SAVED TO DISK \t ]]]")

    restart()


####################################################
# ? FUCNTION TO DOWNLOAD FILES TO MAIN NODE SERVER #
# ? USES TCP PROTOCOL                              #
# ? ACCEPTS FILENAME FROM MAIN NODE SERVER         #
# ? READS THE FILE AND SENDS IT AT ONCE            #
####################################################
def getFileForDownload(endUserCon):
  
    try:
        fileName = endUserCon.recv(1024).decode()
        file = open(fileName, "rb")
        fileData = file.read()
        print("[[[ \t SENDING FILE ... \t ]]]")
        endUserCon.sendall(fileData)
        endUserCon.send(b"[TFROVR]")

        print(f"[[[ \t FILE SENT \t ]]]")
        time.sleep(1)
        file.close()

        restart()

    except Exception as e:
        print(e)


########################################################################
# ? ENTRY POIN FOR STORAGE-SERVER-01                                   #
# ? LISTENS FOR INCCOMING REQUESTS FROM MAIN NODE SERVER               #
# ? ACCEPTS ACTION TYPE FROM MAIN NODE SERVER                          #
# ? IF ACTION IS UPLOAD CALLS SAVE FUCNTION WITH ITS OWN THREAD        #
# ? IF ACTION IS DOWNLOAD, CALLS DOWNLOAD FUCNTION WITH ITS OWN THREAD #
########################################################################

STRSVR01Socket.listen()
print("[[[ \t STRGSVR01 TCP LISTENING ...  \t]]]")

while True:
    endUserCon, endUserAddr = STRSVR01Socket.accept()
    action = endUserCon.recv(1024).decode()
    if action == "upload":
        savingFilesThread = threading.Thread(target=saveFile, args=(endUserCon,))
        savingFilesThread.start()
    elif action == "download":
        getFileForDownloadThread = threading.Thread(
            target=getFileForDownload, args=(endUserCon,)
        )
        getFileForDownloadThread.start()
